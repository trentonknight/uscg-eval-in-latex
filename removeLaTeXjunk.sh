#!/bin/bash
echo "Removing all LaTeX aux, bbl, bcf, blg, idx, log, out , run.xml, and toc files"
rm *.aux
rm *.bbl
rm *.bcf
rm *.blg
rm *.idx
rm *.log
rm *.out
rm *.run.xml
rm *.toc
echo "Removed."
